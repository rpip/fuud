import dj_database_url
from fuud.settings.common import *

#==============================================================================
# Generic Django project settings
#==============================================================================

DEBUG = True
TEMPLATE_DEBUG = DEBUG

LOCAL_DATABASE_URL = 'postgres://fuud:fuud@localhost:5432/fuud'
if not os.getenv('DATABASE_URL'):
    os.environ['DATABASE_URL'] = LOCAL_DATABASE_URL

DATABASES = {
    'default': dj_database_url.config(default=os.getenv('DATABASE_URL'))
}

INSTALLED_APPS += (
    'debug_toolbar',
)

MIDDLEWARE_CLASSES += (
  'debug_toolbar.middleware.DebugToolbarMiddleware',
)
