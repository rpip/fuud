import os
import dj_database_url

from fuud.settings.common import *

#==============================================================================
# Generic Django project settings
#==============================================================================

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
  'default': dj_database_url.config(default=os.getenv('DATABASE_URL'))
}
