import os
import socket
import urlparse

#==============================================================================
# Generic Django project settings
#==============================================================================
HOSTNAME = os.getenv('HOSTNAME', 'localhost')

DEBUG = False
TEMPLATE_DEBUG = DEBUG
INTERNAL_IPS = ('127.0.0.1','::1')

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

SITE_ID = 1

TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
LANGUAGE_CODE = 'en-us'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '!92s(6l)6inop577sumxw-9wc11*220-us_z__wf0%3fpakviz'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'suit',
    'django.contrib.admin',
    # 'django.contrib.admindocs',

    # third-party apps
    'south',
    'gunicorn',
    'django_extensions',
    'imagekit',
    'django_prices',
    'corsheaders',
    'rest_framework',
    'social.apps.django_app.default',
    'django_rq',

    # fuud apps
    'fuud', # core application
    'apps.api'
)

#==============================================================================
# Calculation of directories relative to the project module location
#==============================================================================

ENVIRONMENT = os.getenv('ENVIRONMENT', 'DEVELOPMENT')

PROJECT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))
PROJECT_NAME = PROJECT_PATH.split('/')[-1]


#==============================================================================
# Project URLS and media settings
#==============================================================================

ROOT_URLCONF = 'fuud.urls'

MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, 'static'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

#==============================================================================
# Templates
#==============================================================================

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
    'django.core.context_processors.csrf',
    # 3rd party apps
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
    # custom middleware
    'fuud.context_processors.app_context',
)

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_PATH, 'templates')
)

#==============================================================================
# Middleware
#==============================================================================
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'fuud.middleware.FuudMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

#==============================================================================
# Auth / security
#==============================================================================
#AUTH_USER_MODEL = 'auth.Users'
AUTH_PROFILE_MODULE = 'fuud.models.UserProfile'

AUTHENTICATION_BACKENDS = (
      'social.backends.open_id.OpenIdAuth',
      'social.backends.google.GoogleOpenId',
      'social.backends.google.GoogleOAuth2',
      'social.backends.google.GoogleOAuth',
      'social.backends.twitter.TwitterOAuth',
      'social.backends.yahoo.YahooOpenId',
      'django.contrib.auth.backends.ModelBackend',
  )


SOCIAL_AUTH_TWITTER_KEY = 'AbPXD8iAdcNOIX4Qfp89g'
SOCIAL_AUTH_TWITTER_SECRET = 'ZGF6BFiuXpSX6jbmHq17GVGaR1LZaHk7f8h4wKOiY'
SOCIAL_AUTH_LOGIN_REDIRECT_URL  = '/'
SOCIAL_AUTH_LOGIN_ERROR_URL = '/login-error/'
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/'

#==============================================================================
# Miscellaneous project settings
#==============================================================================
# Application configs
CONFIG_FILES_INPUTDIR = os.path.join(PROJECT_PATH, 'conf')
CONFIG_FILES_OUTPUTDIR = os.path.join(CONFIG_FILES_INPUTDIR, '_output')

# Application cache manifest
MANIFEST_FILENAME = 'offline.appcache'
MANIFEST_TPL_PATH = os.path.join(CONFIG_FILES_INPUTDIR,
                                 MANIFEST_FILENAME)
MANIFEST_FILE_PATH = os.path.join(PROJECT_PATH, 'static',
                                  MANIFEST_FILENAME)


# variable passed as context to the app config files during rendering
CONFIG_FILES_CONTEXT =   {'log_dir': os.path.join(PROJECT_PATH, 'log'),
                          'project_name': PROJECT_NAME,
                          'project': PROJECT_NAME,
                          'program_name': PROJECT_NAME,
                          'env': {},
                          'settings': os.getenv('DJANGO_SETTINGS_MODULE'),
                          'virtualenv_root': os.getenv('VIRTUAL_ENV'),
                          'http_auth': False,
                          'socket': os.path.join(os.path.dirname(PROJECT_PATH),'.sock'),
                          'domain': os.getenv('APP_DOMAIN_NAME', PROJECT_NAME),
                          'public_root': os.getenv('PUBLIC_ROOT',
                                                   '/var/www/' + PROJECT_NAME + '/static'),
                          'ssl_dir': os.path.join(PROJECT_PATH, 'static', 'ssl'),
                          'workers': os.getenv('APP_SERVER_WORKERS', 5)
                         }

# Path to AngularJS templates
ANGULAR_TEMPLATES_DIR = os.path.join(STATIC_ROOT, 'partials')
ANGULAR_TEMPLATE_EXT = 'html'
ANGULAR_TEMPLATE_EXCLUDE = ['categories.html']

APP_NAME = 'Fuud'
APP_TITLE = 'Great food, always!'

# who to email when an order is received
EMAIL_ON_ORDERS = []

# Google Analytics account id
GOOGLE_ANALYTICS_ACCOUNT = None

DEBUG_TOOLBAR_PATCH_SETTINGS = False

# process order as background task if the items exceed limit
ASYNC_CART_LIMIT = 10

# default trading currency
DEFAULT_ITEM_CURRENCY = 'GHC'

# default item stockrecord
DEFAULT_ITEM_STOCK = 5

# item images
THUMBNAIL_SIZE = (500,350)
THUMBNAIL_QUALITY = 100

# Fixme: randomly select the default image from a pool of files
DEFAULT_ITEM_IMAGE = 'stock-photo-6319898-holiday-party.jpg'
DEFAULT_ITEM_IMAGE_PATH = os.path.join('default', DEFAULT_ITEM_IMAGE)

# private data directory
PRIV_DATA_DIR = os.path.join(PROJECT_PATH, 'data')
DEFAULT_RESTAURANT_MENU_FILE = os.path.join(PRIV_DATA_DIR,
                                            'restaurant_menu.txt')


# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'fuud.wsgi.application'

#==============================================================================
# Third party app settings
#==============================================================================
PUSHER_APPID = '63079'
PUSHER_KEY = '6fefddda5af6add29549'
PUSHER_SECRET = '09fcc73acbdd4eb8872b'
PUSHER_CHANNEL = 'orders'

# html output minifying
HTML_MINIFY = True
KEEP_COMMENTS_ON_MINIFYING = True

# use GMail for sending emails
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'my GMail account'
EMAIL_HOST_PASSWORD = 'my GMail password'
DEFAULT_FROM_EMAIL = 'my GMail account'
DEFAULT_TO_EMAIL = 'to email'

redis_url = urlparse.urlparse(os.getenv('REDISTOGO_URL', 'redis://localhost:6379'))

if not DEBUG:
    CACHES = {
        'default': {
            'BACKEND': 'redis_cache.RedisCache',
            'LOCATION': '%s:%s' % (redis_url.hostname, redis_url.port),
            'PREFIX': 'djsession',
            'OPTIONS': {
                'PASSWORD': redis_url.password,
                'DB': 0,
            },
        },
    }


RQ_QUEUES = {
    'default': {
        'HOST': redis_url.hostname,
        'PORT': redis_url.port,
        'DB': 0,
    },
    'high': {
        'HOST': redis_url.hostname,
        'PORT': redis_url.port,
        'DB': 0,
    },
    'low': {
        'HOST': redis_url.hostname,
        'PORT': redis_url.port,
        'DB': 0,
    }
}

# Restrict AJAX CORS to '/api'
CORS_ORIGIN_ALLOW_ALL = True
CORS_URLS_REGEX = r'^/api/.*$'
CORS_ALLOW_CREDENTIALS = True

# Django Suit configuration
SUIT_CONFIG = {
    # header
    'ADMIN_NAME': 'Fuud',
    # 'HEADER_DATE_FORMAT': 'l, j. F Y',
    # 'HEADER_TIME_FORMAT': 'H:i',

    # forms
    # 'SHOW_REQUIRED_ASTERISK': True,  # Default True
    # 'CONFIRM_UNSAVED_CHANGES': True, # Default True

    # menu
    # 'SEARCH_URL': '/admin/auth/user/',
    # 'MENU_ICONS': {
    #    'sites': 'icon-leaf',
    #    'auth': 'icon-lock',
    # },
    # 'MENU_OPEN_FIRST_CHILD': True, # Default True
    # 'MENU_EXCLUDE': ('auth.group',),
    # 'MENU': (
    #     'sites',
    #     {'app': 'auth', 'icon':'icon-lock', 'models': ('user', 'group')},
    #     {'label': 'Settings', 'icon':'icon-cog', 'models': ('auth.user', 'auth.group')},
    #     {'label': 'Support', 'icon':'icon-question-sign', 'url': '/support/'},
    # ),

    # misc
    # 'LIST_PER_PAGE': 15
}

# REST API
REST_FRAMEWORK = {
    # Use hyperlinked styles by default.
    # Only used if the `serializer_class` attribute is not set on a view.
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework.filters.DjangoFilterBackend',
    ),
    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.ModelSerializer',

    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    #'DEFAULT_PERMISSION_CLASSES': [
    #    'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    #]
    'PAGINATE': 25,
}

#==============================================================================
# Logging
#==============================================================================

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
