/**
 * Application data stores
 *
 * @module Fuud.services
 */

'use strict';


angular.module('Fuud.services', ['ngResource'])
/**
 * Resource endpoint for the items to display.
 * Pulls data from the REST API
 *
 * @class Item
 */
.factory('Item', ['$rootScope', '$resource',  function ($rootScope, $resource) {
    return $resource($rootScope.API.items, {cache: true}, {
        query: {
            method: 'GET',
            isArray: false
        }
    })
}])
/**
 * Data source to lazy load the Item resources.
 *
 *
 * @class ItemStore
 */
.factory('ItemStore', ['Item', '$rootScope', function(Item, $rootScope) {
    return function() {
        return $rootScope.loadItems(Item, 'fuud_api_items');
    };
}])

/**
 * Data store for the items to display.
 * Pulls data from the REST API
 *
 * @class Order
 */
.factory('Order', ['$rootScope', '$resource',  function ($rootScope, $resource) {
    return $resource($rootScope.API.orders, null, {
        query: {
            method: 'GET',
            isArray: false
        }
    })
}])
/**
 * Data source to lazy load the Order resources.
 *
 *
 * @class OrderStore
 */
.factory('OrderStore', ['Order', '$rootScope', function(Order, $rootScope) {
    return function() {
        return $rootScope.loadItems(Order, 'fuud_api_orders');
    };
}])
/**
 * Application-wide cache.
 * Be default, it uses the localStorage for data persistence.
 *
 * @class Cache
 */
.service('Cache', ['$angularCacheFactory', function ($angularCacheFactory) {
    return $angularCacheFactory('Cache');
}]);
