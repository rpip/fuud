/**
 * Application directives
 *
 * @module Fuud.directives
 */

'use strict';


angular.module('Fuud.directives', []).
directive('appVersion', ['version', function(version) {
   return function(scope, elm, attrs) {
     elm.text(version);
   };
}])
.directive('watchitemqty', ['$log', '$rootScope', function($log, $rootScope) {
    return {
        restrict: 'EA',
        link: function (scope, element, attrs){
            element.on('change', function(){
                var itemid = attrs['itemid'],
                    qty = element.val();
                $log.debug("itemid " + itemid + " | qty = " + qty);
                $rootScope.updateCartItem(itemid, qty);
            });
        }
    }
}]);
