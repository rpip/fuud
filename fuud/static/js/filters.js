/**
 * Application filters
 *
 * @module Fuud.filters
 */

'use strict';


angular.module('Fuud.filters', [])
/**
 * Returns interpolated version string
 *
 * @method interpolate
 * @returns String
 */
.filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    }
  }])
/**
 * Returns quantity of the item in the current cart
 *
 * @method qty_in_cart
 * @returns Integer
 */
.filter('qty_in_cart', ['$rootScope', function($rootScope) {
    return function(item) {
        return $rootScope.getItemInCart(item).qty;
    }
}])
/**
 * Returns the subtotal of the item in the current cart
 *
 * @method subtotal_in_cart
 * @returns Integer
 */
.filter('subtotal_in_cart', ['$rootScope', function($rootScope) {
    return function(item) {
        return $rootScope.getItemInCart(item).total;
    }
}])
/**
 * Returns unique elements from the list
 *
 * @method unique
 * @returns Array
 */
.filter('unique', function() {
    return function(list) {
        return _.uniq(list, function(x){
            return x.id
        });
    }
})
/**
* Returns the count of orders by the specified status
*
* @method order_status_count
* @returns Integer
*/
.filter('order_status_count', ['$rootScope', function($rootScope) {
    return function(status) {
        return $rootScope.getOrdersByStatus(status);
    }
}])
/**
 * Returns the text/name value of the order status
 *
 * @method order_status_name
 * @returns Integer
 */
.filter('orderStatusName', ['$rootScope', function($rootScope) {
    return function(value) {
        return _.find($rootScope.OrderStatusOptions, function(x){
            console.log(x);
            return x.value === value;
        });
    }
}])
/**
 * Truncates a string to the specified length.
 * Default length is 50
 *
 * @method truncate
 * @returns Integer
 */
.filter('truncate', function() {
    return function (text, length, end) {
        length = length || 50;
        end = end || '...';
        if (text.length <= length || text.length - end.length <= length) {
            return text;
        }
        else {
            return String(text).substring(0, length-end.length) + end;
        }
    };
});
