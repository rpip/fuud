/**
 *  Application controllers
 *
 *  @module Fuud.controllers
 */

'use strict';

angular.module('Fuud.controllers', ['Fuud.services'])
/**
 * Provides actions/variables related to the menu items
 *
 * @class ItemCtrl
 */
.controller('ItemCtrl', ['$rootScope', '$scope', '$log', 'Cache', 'items',
                          function($rootScope, $scope, $log, Cache, items) {
  $rootScope.currentPage = 'home';
  $scope.items = items

 /**
  * Retrieve a single item
  *
  * @method getItemById
  * @param {Integer} Item id
  * @return {Boolean} ItemId If found, returns an 'item', else null
  */
  $scope.getItemById = function (ItemId){
      var found = $.grep($scope.items, function(item){
          return item.id == parseInt(ItemId);
      });
      return found ? found[0] : null;
  };


 /**
  * Add item to cart, and emits an application-wide
  * 'cartUpdated' message.
  *
  * @method AddToCart
  * @param {Integer} Item id
  * @return {Boolean} ItemId Returns true on success
  */
  $scope.AddToCart = function(ItemId){
      var Item = $scope.getItemById(ItemId);
      if (Item){
          $rootScope.cart.items.push(Item);
          $scope.$emit('cartUpdated');
          // notification message
      }
      // log error for item not found
  };
}])
/**
 * Provides actions/variables related to the user account.
 *
 * @class AccountCtrl
 */
.controller('AccountCtrl', [
    '$rootScope',
    '$scope',
    '$log',
    'Cache','orders',
    '$q',
    '$http',
    '$cookieStore',
function($rootScope, $scope, $log, Cache, orders, $q, $http, $cookieStore) {
   $rootScope.currentPage = 'account';
   $scope.user.orders.items = orders;
   //$log.debug(orders);
   $scope.user.orders.total = orders.length;

   $scope.updateUserInfo = function(){
       var username = $rootScope.user.username;
       $rootScope.API.users + username + '/update_info';
       var email = $rootScope.stripIt($rootScope.user.email);
       var address = $rootScope.stripIt($rootScope.user.address);
       var phone_number = $rootScope.stripIt($rootScope.user.phone_number);
       var keepUpdated = $rootScope.user.keepUpdated;

       var data = {
           address: address,
           email: email,
           phone: phone_number,
           subscribed: keepUpdated,
       };
       var delay = $q.defer();
       var url = $rootScope.API.users + username + '/update_info/';
       $http.post(url, data)
       .success( function (response) {
           $cookieStore.put('fuud_address', address);
           $cookieStore.put('fuud_email', email);
           $cookieStore.put('phone_number', phone_number);
           $cookieStore.put('subscribed', keepUpdated);
           $rootScope.safeApply(function(){
               $rootScope.toggleFlashMessage("Account info updated");
           });
           $rootScope.displayModal("Accounts", "Your account has \
               been successfully updated");
           delay.resolve(response);
       })
       .error( function() {
           var errMsg = "There was error updating your account info";
           $rootScope.safeApply(function(){
               $rootScope.toggleFlashMessage(errMsg);
           });
           delay.reject(errMsg);
       });
       return delay.promise;
   }

}])
/**
 * Admin page controller
 *
 * @class AdminCtrl
 */
.controller('AdminCtrl', ['$rootScope', '$scope', '$log', 'orders',
    function($rootScope, $scope, $log, orders) {
    $rootScope.currentPage = 'dashboard';
    $scope.orders.items = orders;
    $scope.orders.total = orders.length;
}])
/**
 * Provides actions/variables for handling cart checkout
 *
 * @class CheckoutCtrl
 */
.controller('CheckoutCtrl', ['$rootScope', '$scope', '$http', '$log',
function($rootScope, $scope, $http, $log) {
   $rootScope.currentPage = 'checkout';
   /**
    * Submit the order.
    * This send a POST request to the checkout API
    *
    * @method submitOrder
    * @return {Boolean} Returns true on success
    */
   $rootScope.submitOrder = function(){
       var itemCount = _.countBy($rootScope.cart.items, function(item){
           return item.id;
       });

       var data = {
           items: itemCount,
           username: $rootScope.user.username
       };
       $http.post($rootScope.API.checkout, data)
           .success(function(resp){
               var ref;
               if(resp.data){
                   ref = "Your order reference is " + resp.data.ref;
               }
               else{
                   ref = "You will be notified when it is ready";
               }
               $rootScope.toggleFlashMessage("success", "Order has been submitted. " + ref);
               $rootScope.emptyCart(true);
           })
           .error(function(error){
               $rootScope.toggleFlashMessage("error", "Failed to submit the order");
           });
   };
}])
/**
 * Provides actions/variables needed by the Help page template
 *
 * @class HelpCtrl
 */
.controller('HelpCtrl', ['$rootScope', function($rootScope) {
   $rootScope.currentPage = 'help';
}])
/**
 * URL '404' error handler
 *
 * @class ErrorCtrl
 */
.controller('ErrorCtrl', ['$rootScope',function($rootScope) {
    $rootScope.currentpage = 'error';
}]);
