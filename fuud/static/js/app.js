/**
 * The main application entry point.
 * This module configure the URL patterns, and global/shared functions and
 * variables.
 *
 * @module Fuud
 */

'use strict';

var Fuud = angular.module('Fuud', [
    'ngRoute',
    'ngCookies',
    'ngResource',
    'ngProgress',
    'Fuud.filters',
    'Fuud.services',
    'Fuud.directives',
    'Fuud.controllers',
    'jmdobry.angular-cache'
])
.config(['$routeProvider', '$locationProvider', '$angularCacheFactoryProvider',
 function($routes, $location, $angularCacheFactoryProvider) {
     //set default cache options
     // optionally set cache defaults
     $angularCacheFactoryProvider.setCacheDefaults({
         // This cache can hold 1000 items
         capacity: 1000,

         // Items added to this cache expire after 15 minutes
         maxAge: 900000,

         // Items will be actively deleted when they expire
         deleteOnExpire: 'aggressive',

         // This cache will check for expired items every minute
         recycleFreq: 60000,

         // This cache will clear itself every hour
         cacheFlushInterval: 3600000,

         // This cache will sync itself with localStorage
         storageMode: 'localStorage',

         // Custom implementation of localStorage
         //storageImpl: myLocalStoragePolyfill,

         // Full synchronization with localStorage on every operation
         verifyIntegrity: true,

         // This callback is executed when the item specified by "key" expires.
         // At this point you could retrieve a fresh value for "key"
         // from the server and re-insert it into the cache.
         onExpire: function (key, value) {

         }
     });
     //$location.html5Mode(true);
     $routes
        .when('/', {
            templateUrl: 'items.html',
            controller: 'ItemCtrl',
            resolve: {
                items: ["ItemStore", function(ItemStore) {
                    return ItemStore();
                }]
            }
        })
        .when('/checkout', {
            templateUrl: 'checkout.html',
            controller: 'CheckoutCtrl'
        })
        .when('/dashboard', {
            templateUrl: 'dashboard.html',
            controller: 'AdminCtrl',
            resolve: {
                orders: ["OrderStore", function(OrderStore) {
                    return OrderStore();
                }]
            }
        })
        .when('/account', {
            templateUrl: 'account.html',
            controller: 'AccountCtrl',
            resolve: {
                orders: ["$http", '$q', '$log', '$cookies', '$rootScope',
                         function($http, $q, $log, $cookies, $rootScope) {
                    var delay = $q.defer();
                    //+ $cookies.fuud_username + '/orders'
                    var username = $rootScope.user.username;
                    var url = $rootScope.API.orders + username + '/orders';
                    $http.get(url).success( function (response) {
                        delay.resolve(response);
                    })
                    .error( function() {
                        $log.error("Failed to fetch user orders");
                        delay.reject("Failed to fetch user orders");
                    });
                    return delay.promise;
                }]
            }
        })
        .when('/help', {
            templateUrl: 'help.html',
            controller: 'HelpCtrl'
        })
        .when('/notfound', {
            templateUrl: 'notfound.html',
            controller: 'ErrorCtrl'
        })
        .otherwise({
            redirectTo: '/notfound'
        });
}])
.run(['$rootScope',
      '$location',
      'ngProgress',
      'Cache',
      '$log',
      '$cookies',
      '$http',
      '$cookieStore',
      '$q',
      '$injector',
function($rootScope, $location, ngProgress, Cache, $log,
         $cookies, $http, $cookieStore, $q, $injector){

    // HTTP request CSRF token
    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];

    /**
     * Loads the items required for the template and controller.
     * The items are loaded from the REST API, if not found in the cache.
     *
     *
     * @method loadItems
     * @return {Boolean} Returns true on success
     */
    $rootScope.loadItems = function (Resource, cacheKey) {
        var delay = $q.defer();

        if (!Cache.get(cacheKey)) {
            $log.info("Loading resource from server... : " + cacheKey);
            Resource.query(function(response) {
                Cache.put(cacheKey, response.data);
                delay.resolve(response.data);
            }, function() {
                delay.reject('Unable to fetch resource: ' + cacheKey);
            });
        }  else {
            $log.log('Loading resource from local cache..: ' + cacheKey);
            delay.resolve(Cache.get(cacheKey));
        }
        return delay.promise;
    };

    $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };


    /**
     * Application-wide modal box object.
     *
     *
     * @property
     * @type Object
     */
    $rootScope.modal = {
        title: "Hello, World!",
        body: "foobar"
    }

   /**
     * Application-wide modal box controller.
     * This function sets the modal box title and body, thus
     * making it visible.
     *
     * @property
     * @type function
     */
    $rootScope.displayModal = function (title, body) {
        $rootScope.modal.title = title;
        $rootScope.modal.body = body;
        $("#modal-box").modal({show: true});
    }


    /**
     * Application-wide modal box controller.
     * This function sets the modal box title and body to 'null', thus
     * making it hidden.
     *
     * @property
     * @type function
     */
    $rootScope.hideModal = function (title, body) {
        $rootScope.modal.title = null;
        $rootScope.modal.body = null;
    }

    /**
     * Application server configs
     *
     *
     * @property
     * @type Object
     */
    $rootScope.APP = new function(){
        this.debug =  true,
        // REST API Server
        this.apiServer = "/api"
    };

    /**
     * Application REST API config
     *
     *
     * @property
     * @type Object
     */
    $rootScope.API = {
        items: $rootScope.APP.apiServer + '/items/?format=json',
        checkout: $rootScope.APP.apiServer + '/orders/checkout/?format=json',
        orders: $rootScope.APP.apiServer + '/orders/',
        users: $rootScope.APP.apiServer + '/users/'
    };

    /**
     * The global Flash messages
     *
     *
     * @property
     * @type Object
     */
    $rootScope.flash = {
        warning:  null,
        info: null,
        error: null,
        success: null
    }

    /**
     * Toggle display of the currently visible flash message
     *
     *
     * @method
     * @param String type Flash message type
     */
    $rootScope.toggleFlashMessage = function (type, message){
        if (message) {
            return $rootScope.flash[type] = message;
        }
        return $rootScope.flash[type] = null;
    }

    /**
     * The currently logged-in user
     *
     *
     * @property
     * @type String
     * @default null
     */
    if($cookies.fuud_is_authenticated){
        $rootScope.user = {
            isAuthenticated: parseInt($cookies.fuud_is_authenticated),
            isAdmin: parseInt($cookies.fuud_is_admin),
            isStaff: parseInt($cookies.fuud_is_staff),
            username: $cookies.fuud_username,
            email: $cookies.fuud_email,
            address:  $cookies.fuud_address,
            phone_number: $cookies.fuud_phone_number,
            keepUpdated: parseInt($cookies.fuud_subscribed_to_updates),
            orders: {
                items: [],
                total: 0
            }
        }
    }else{
        $rootScope.user = {};
    }

    /**
     * Session cleanup upon logging out.
     * Deletes the currently logged-in user's cookie data,
     * and other session-bound data.
     *
     * @method logOut
     */
    $rootScope.logOut = function(){
        $log.info("Loggig out of the current session...");
        $rootScope.user = null;
        $cookieStore.remove('fuud_subscribed_to_updates');
        $cookieStore.remove('fuud_is_authenticated');
        $cookieStore.remove('fuud_is_admin');
        $cookieStore.remove('fuud_is_staff');
        $cookieStore.remove('fuud_email');
        $cookieStore.remove('fuud_phone_number');
        $cookieStore.remove('fuud_username');
        $cookieStore.remove('fuud_address');
        $log.info("All cookie data successfully cleared...");
    }

    /**
     * The default currency unit
     *
     * @property currencyUnit
     * @type String
     * @default "GHC"
     */
    $rootScope.currencyUnit = 'GHC ';

    /**
     * Item image placeholder, as a binary blob data.
     * Useful for when the item has not cover image.
     *
     * @property binaryItemThumbnail
     * @type String
     * @default "binary data"
     */
    $rootScope.binaryItemThumbnail = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUoAAADcCAYAAAAWYejvAAAN0UlEQVR4Xu3cB48cRRMG4DbJZJNzzkFEkUSQkPjfCBkQUWQQSeQcTBYZf6qR5rTfgQ1zfXXeq35Gsnzene7teur0atJ6z4EDBw42GwECBAgcUmCPoPTbQYAAgcMLCEq/IQQIEPgXAUHpV4QAAQKC0u8AAQIE+gQcUfb5GU2AwAACgnKAJiuRAIE+AUHZ52c0AQIDCAjKAZqsRAIE+gQEZZ+f0QQIDCAgKAdoshIJEOgTEJR9fkYTIDCAgKAcoMlKJECgT0BQ9vkZTYDAAAKCcoAmK5EAgT4BQdnnZzQBAgMICMoBmqxEAgT6BARln5/RBAgMICAoB2iyEgkQ6BMQlH1+RhMgMICAoBygyUokQKBPQFD2+RlNgMAAAoJygCYrkQCBPgFB2ednNAECAwgIygGarEQCBPoEBGWfn9EECAwgICgHaLISCRDoExCUfX5GEyAwgICgHKDJSiRAoE9AUPb5GU2AwAACgnKAJiuRAIE+AUHZ52c0AQIDCAjKAZqsRAIE+gQEZZ+f0QQIDCAgKAdoshIJEOgTEJR9fkYTIDCAgKAcoMlKJECgT0BQ9vkZTYDAAAKCcoAmK5EAgT4BQdnnZzQBAgMICMoBmqxEAgT6BARln5/RBAgMICAoB2iyEgkQ6BMQlH1+RhMgMICAoBygyUokQKBPQFD2+RlNgMAAAoJygCYrkQCBPgFB2ednNAECAwgIygGarEQCBPoEBGWfn9EECAwgICgHaLISCRDoExCUfX5GEyAwgICgHKDJSiRAoE9AUPb5GU2AwAACgnKAJiuRAIE+AUHZ52c0AQIDCAjKAZqsRAIE+gQEZZ+f0QQIDCAgKAdoshIJEOgTEJR9fkYTIDCAgKAcoMlKJECgT0BQ9vkZTYDAAAKCcoAmK5EAgT4BQdnnZzQBAgMICMoBmqxEAgT6BARln5/RBAgMICAoB2iyEgkQ6BMQlH1+RhMgMICAoBygyUokQKBPQFD2+RlNgMAAAoJygCYrkQCBPgFB2ednNAECAwgIygGarEQCBPoEBGWfn9EECAwgICgHaLISCRDoExCUfX5GEyAwgICgHKDJSiRAoE9AUPb5GU2AwAACgnKAJiuRAIE+AUHZ52c0AQIDCAjKAZqsRAIE+gQEZZ+f0QQIDCAgKAdoshIJEOgTEJR9fkYTIDCAgKAcoMlKJECgT0BQ9vkZTYDAAAKCcoAmK5EAgT4BQdnnZzQBAgMICMoBmqxEAgT6BARln5/RBAgMICAoB2iyEgkQ6BMQlH1+RhMgMICAoBygyUokQKBPQFD2+RlNgMAAAoJygCYrkQCBPgFB2ednNAECAwgIygGarEQCBPoEBGWfn9EECAwgICgHaLISCRDoExCUfX5GEyAwgICgHKDJSiRAoE9AUPb5GU2AwAACgnKAJm8u8ffff2/vvvtu+/LLL9uvv/7ajjnmmHbGGWe0K664op144ol/Ezl48GD78MMP28cff9x+/vnndvTRR0/7X3nllduy/9IWzOv/6quv2i+//NKOOuqodtJJJ7WLL764nXfeeWu//qX12v/ICwjKI9+DHV3Bb7/91p555pkp8DZvEZh33HFHO/nkk//vrddee6198sknf9v/2GOPbXfeeeffwnLp/ksAIhifffbZKSD/aYuwvPbaa9d2/Utqte/6CAjK9enFjqzkzTffbB988MH0WWeeeWY755xz2tdff92++OKL6bXTTjttCst5+/bbb6dgim3v3r3tsssua99991377LPPptfOOuusduutt255/6VFv/rqq+3TTz+dhu3bt6+df/75U2jGEe+ff/45vX7XXXe1U089dfp53da/tF77r4eAoFyPPuzYKp544on2008/tRNOOKHde++9bc+ePS1OrR999NEWR5uxPfTQQ9Prsa0eHd5yyy3t7LPPnl6f54n97r///ilEt7L/ksJjnY888sgUiHH0G58bf8f2zjvvTH9ii0sI8Wcr61la75L123f3CgjK3du7La88Trv/+OOPdsopp0xzRADt37+/xbW/CJ4HH3xwY+7HH3984zQ9Xp+D6Y033piO4mK7+eabpyPT2JbsH0eGcYQYW1z3vOeee6YA3xxwcT309ttvn9YcnzlfV73qqqs21hlHuK+88sr07zjqnd9bsp6l699yAwzcdQKCcte1bPsW/Ndff01Hl++99177/PPPp4lXj8bi/Ycffnh6fXOAxpi33357ei9u6lx++eVt6f4x9qWXXto47Y9LAbfddtt0KeD555+f5o7roHfffXc7/vjjD1v46pHgjTfeOJ2SL13P0v23rxNmWncBQbnuHUpcX9z1fvHFFzc+IcIuQm/e4lQ8jjRji1PrBx54YOO9OLKLo8rYLrnkknbNNddMp+5L9o+xcRT71FNPbdyciRsxEcJx1BjbTTfd1M4999zDKnzzzTftueeem46MI9Dvu+++KWCXrmfp/omtMfWaCQjKNWvITi4nburEzZ15i0dsIvDiyC62uEny2GOPTT/HEV1cE5y3jz76qL3++uvTPy+66KJ23XXXLd5/nuvAgQNT0G3eLrjggnbDDTccliRu1rzwwgvTaXls119/fbvwwgt3dP072TOfdWQEBOWRcV+LT40jqHgGMa5Zxilw/B03Z+JUNx4RWnqEtXT/VYTVa57xelyrjHXM10T/CSxO0WPd893uCMgIynlbup6l+69FEy1iRwQE5Y4wr/+HrN4MmZ9FXHrNbun+qyo//vhje/LJJzdeitPtOO0+1BYPm0dIxmeuHtWu7r90PUv3X/+uWuF2CQjK7ZLcBfNEEPzwww/TKXJcw4u7yfO2egMlHgGKR4Fii1Pv+eHu1bvecdodp9+xrd71Xrp/jI9ri/GsZjyfubqtzrv6elyTjJs9c0heeuml7eqrr/7HDixdz9L9d0HbLXEbBATlNiDuliniBkk8LxlbfFUxHseJU+/Y3nrrrfb+++9PP69+u+Xll1/euCO+Glzzc5Sxf9zkmZ+jXLp/jF+9gx7XQudgPu6446Y1xt/zFu/FzZ+4CRTb4UIy3l+6nqX775beW2efgKDs89t1o+NILI4eYzv99NOnO8px2jsfHa5eo4x9Vo8052/mfP/99xvfjtn8zZyl+8dnP/3009PRYTxLGdcl43rlvMbVo9vNwRf/jvXPD8fPzYibUfF40E6sf9f9AljwlgQE5ZbYdu+gOCKL73rPj99srmT1rvH83urXBlf3P9R3vf/r/hGOEZIRlrHF6XMcIcYa43rl5jvZq3fhD9eB+XGl7PXv3t8CK18qICiXihXYP+7uxuluPEcZ4RN3luN70/GNlviu9+YtriHGEWf8mf/3oDgajW+/HOp/G/ov+6+e7sd3s+M/2JiPDlef04z1xZFmXJuMB8v/bdsclFnr/7d1eL+OgKCs00uVECCQJCAok2BNS4BAHQFBWaeXKiFAIElAUCbBmpYAgToCgrJOL1VCgECSgKBMgjUtAQJ1BARlnV6qhACBJAFBmQRrWgIE6ggIyjq9VAkBAkkCgjIJ1rQECNQREJR1eqkSAgSSBARlEqxpCRCoIyAo6/RSJQQIJAkIyiRY0xIgUEdAUNbppUoIEEgSEJRJsKYlQKCOgKCs00uVECCQJCAok2BNS4BAHQFBWaeXKiFAIElAUCbBmpYAgToCgrJOL1VCgECSgKBMgjUtAQJ1BARlnV6qhACBJAFBmQRrWgIE6ggIyjq9VAkBAkkCgjIJ1rQECNQREJR1eqkSAgSSBARlEqxpCRCoIyAo6/RSJQQIJAkIyiRY0xIgUEdAUNbppUoIEEgSEJRJsKYlQKCOgKCs00uVECCQJCAok2BNS4BAHQFBWaeXKiFAIElAUCbBmpYAgToCgrJOL1VCgECSgKBMgjUtAQJ1BARlnV6qhACBJAFBmQRrWgIE6ggIyjq9VAkBAkkCgjIJ1rQECNQREJR1eqkSAgSSBARlEqxpCRCoIyAo6/RSJQQIJAkIyiRY0xIgUEdAUNbppUoIEEgSEJRJsKYlQKCOgKCs00uVECCQJCAok2BNS4BAHQFBWaeXKiFAIElAUCbBmpYAgToCgrJOL1VCgECSgKBMgjUtAQJ1BARlnV6qhACBJAFBmQRrWgIE6ggIyjq9VAkBAkkCgjIJ1rQECNQREJR1eqkSAgSSBARlEqxpCRCoIyAo6/RSJQQIJAkIyiRY0xIgUEdAUNbppUoIEEgSEJRJsKYlQKCOgKCs00uVECCQJCAok2BNS4BAHQFBWaeXKiFAIElAUCbBmpYAgToCgrJOL1VCgECSgKBMgjUtAQJ1BARlnV6qhACBJAFBmQRrWgIE6ggIyjq9VAkBAkkCgjIJ1rQECNQREJR1eqkSAgSSBARlEqxpCRCoIyAo6/RSJQQIJAkIyiRY0xIgUEdAUNbppUoIEEgSEJRJsKYlQKCOgKCs00uVECCQJCAok2BNS4BAHQFBWaeXKiFAIElAUCbBmpYAgToCgrJOL1VCgECSgKBMgjUtAQJ1BARlnV6qhACBJAFBmQRrWgIE6ggIyjq9VAkBAkkCgjIJ1rQECNQREJR1eqkSAgSSBARlEqxpCRCoIyAo6/RSJQQIJAkIyiRY0xIgUEdAUNbppUoIEEgSEJRJsKYlQKCOgKCs00uVECCQJCAok2BNS4BAHQFBWaeXKiFAIElAUCbBmpYAgToCgrJOL1VCgECSgKBMgjUtAQJ1BARlnV6qhACBJIH/Aa00jwJpZKODAAAAAElFTkSuQmCC";

    // the shopping cart
    if (Cache.get('cart') ){
        $rootScope.cart = Cache.get('cart');
    }
    else {
        $rootScope.cart = {
            total: null,
            items: []
        };
    }

    /**
     * Maximum quantity of a cart item
     *
     * @property maxItemQty
     * @type Integer
     * @default 10
     */
    $rootScope.maxItemQty = 10;

    /**
     * Minimum quantity of a cart item
     *
     * @property minItemQty
     * @type Integer
     * @default 0
     */
    $rootScope.minItemQty = 0;

    /**
     * Empty the current cart.
     * Deletes all items in the cart.
     * It can also, optionally, delete the cart data from the local cache.
     *
     * @method emptyCart
     * @param Boolean deleteCache Delete cart data from the local cache
     */
    $rootScope.emptyCart = function (deleteCache){
        $rootScope.cart.items = [];
        $rootScope.cart.total = 0.0;
        return deleteCache ? Cache.remove('api.items') : true;
    };


    /**
     * Remove item from the cart
     *
     * @method removeFromCart
     * @param Integer itemId the id of the item to delete
     * @returns Boolean
     */
    $rootScope.removeFromCart = function (itemId, name){
        var items = _.reject($rootScope.cart.items, function (item){
            return item.id == itemId;
        });
        $rootScope.cart.items = items;
        $rootScope.toggleFlashMessage("info", name + " has been removed from the cart");
        $rootScope.$emit('cartUpdated');
    }

    /**
     * Calculates the total cost of the items in the cart.
     * It also updates the global cart.total property.
     *
     * @method calculateCartTotal
     */
    $rootScope.calculateCartTotal = function(){
        var total = 0, idx, itemPrice;
        for (idx = 0; idx < $rootScope.cart.items.length; idx++){
            itemPrice = parseFloat($rootScope.cart.items[idx].price);
            total += itemPrice;
        }
        $rootScope.cart.total = total;
    };

    /**
     * Returns true if the cart is empty
     *
     * @method isCartEmpty
     * @returns Boolean
     */
    $rootScope.isCartEmpty = function(){
        return $rootScope.cart.items.length == 0;
    };

    /**
     * Returns true if item is in cart
     *
     * @method isInCart
     * @param Integer itemId the item id
     */
    $rootScope.isInCart = function (itemId){
        return _.find($rootScope.cart.items, function (item){
            return item.id == itemId;
        });
    }

    /**
     * Returns the item if found in the cart.
     * Returned item has subtotal, and qty as reflected in the cart
     *
     * @method getItemInCart
     * @param Object Item the item to look for in the cart
     * @returns Object the modified item
     */
    $rootScope.getItemInCart = function(item){
        var found = $.grep($rootScope.cart.items, function(itemInCart){
            return item.id == itemInCart.id;
        });
        if (found){
            var idx;
            var itemTotal = 0;
            var itemQty = 0;
            for (idx = 0; idx < found.length; idx++){
                itemQty += 1;
                itemTotal +=  parseFloat(found[idx].price);
            }
            item.qty = itemQty;
            item.total = itemTotal;
            return item;
        }
        return null
    };

    $rootScope.updateCartItem = function (itemId, newQty) {
        var items = $rootScope.cart.items;
        var item = _.find(items, function(item){
            return item.id == parseInt(itemId);
        });
        var others = _.difference(items, item);
        var qty = parseInt(newQty);
        if (qty < 1){
            $rootScope.safeApply(function () {
                $rootScope.cart.items = others;
                $rootScope.$emit('cartUpdated');
            })
        } else {
            for (var i = 0; i < qty; i++){
                others.push(item);
            }
            $rootScope.safeApply(function () {
                $rootScope.cart.items = others;
                $rootScope.$emit('cartUpdated');
            });
        }
        $rootScope.toggleFlashMessage("Cart updated");
        return true;
    }
    /**
     * The order history of currently logged-in user
     *
     *
     * @property
     * @type Object
     */
    $rootScope.orders = {
        total: null,
        items: []
    };


    $rootScope.updateOrderStatus = function (status, ref) {
        var delay = $q.defer();
        var url = $rootScope.API.orders + ref + '/update_status';
        var newStatus;
        if(status == 'cancelled'){
            newStatus = 2;
        } else if (status == 'pending' ) {
            newStatus = 0;
        }
        else if (status == 'processed') {
            newStatus = 1;
        }
        else{
            $log.error('Wrong params for updateOrderStatus: status=' * status);
            return;
        }
        $http.get(url, { params: {'status': newStatus}}).success( function (response) {
            _.each($rootScope.orders.items, function (order) {
                if (order.ref == ref){
                    $rootScope.safeApply(function(){
                        order.status = newStatus;
                    });
                    return true;
                }
                return false;
            })
            $rootScope.displayModal("Order cancellation",
                                    "order #" + ref +
                                    " has been successfully changed to " + status);
            delay.resolve(response);
        }).error( function() {
            $rootScope.displayModal("Order cancellation",
                                    "There was an error cancelling order: " + ref);
            delay.reject("Failed to fetch user orders");
        });
        return delay.promise;
    }

    /**
     * List of valid order status options
     *
     *
     * @property OrderStatusOptions
     * @type Object
     */
    $rootScope.orderStatusOptions = [
        {
            name: 'Pending',
            value: 0
        },
        {
            name: 'Processed',
            value: 1
        },
        {
            name: 'Cancelled',
            value: 2
        }
    ]


    /**
     * Returns 'selected' if the first param(order status)
     * matches the second param
     *
     *
     * @property isOrderStatusSelected
     * @type Object
     */
    $rootScope.isOrderStatusSelected = function (order_status, status){
       if(order_status == status){
           return 'selected'
       }
    }

    // strip all quotes from string
    $rootScope.stripIt = function (x){
        return x.replace(/['"]/g,'');
    };

    if($rootScope.user.isAuthenticated){
        // load the orders
        //loadOrders();
    }


    $rootScope.$on('cartUpdated', function() {
        $rootScope.calculateCartTotal();
        Cache.put('cart', $rootScope.cart);
     });

    $rootScope.$on('cacheUpdated', function() {
        $log.info("Application updated; destroying local cache");
        Cache.destroy();
    });

    $rootScope.progressbar = {
       display: false
    }

    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        $rootScope.progressbar.display = true;
        ngProgress.start();
    });


    // Progress bar
    $rootScope.$on("$routeChangeSuccess", function (event, current, previous, rejection) {
        $rootScope.progressbar.display = false;
        ngProgress.complete();
    });

    // handle route errors
    $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
        //change this code to handle the error somehow
        $log.debug('route change error');
        //$location.path('/notfound').replace();
    });

    /**
     * Initialize the PUSHER connection.
     * Also, binds to a designated channel for events.
     *
     * @property setupPusher
     * @type method
     */
    $rootScope.setupPusher = function (){
      $rootScope.pusher_key = $('#pusher-key').val()
      $rootScope.pusher_channel = $('#pusher-channel').val()
      $rootScope.pusher = new Pusher($rootScope.pusher_key);
      $rootScope.channel = $rootScope.pusher.subscribe($rootScope.pusher_channel);
      $log.info('Connecting to PUSHER...');

      $rootScope.pusher.connection.bind('connected',
                                        $rootScope.onChannelConnected);

      $rootScope.channel.bind('new_order',
                              $rootScope.onChannelEvent);
    }

    /**
     * Callback for successful connection to PUSHER
     *
     *
     * @property onChannelConnected
     * @type method
     */
    $rootScope.onChannelConnected  = function () {
        $rootScope.safeApply(function(){
            $log.info("Connected to PUSHER!");
        });
    }

    /**
     * Channel events callback
     *
     *
     * @property onChannelEvent
     * @type method
     */
    $rootScope.onChannelEvent = function(data) {
        $log.info("Listening to to PUSHER!");
        $log.debug('An event was triggered with message: ' + data.message);
        $rootScope.displayModal("New Order", data.message);
        $rootScope.toggleFlashMessage(data.message);
        $http.get($rootScope.API.orders)
        .success(function(response){
            $log.info('Removing old orders data');
            Cache.remove('fuud_api_orders');
            $log.info(Cache.get('fuud_api_orders'));
            Cache.put('fuud_api_orders', response.data);
            $log.info(response.data);
            $rootScope.toggleFlashMessage('A new order has been added to the dashboard');
            $log.info('Updating orders');
        })
        .error(function(){
            $log.error('There was error ');
        });
    }


    // initialize PUSHER
    if($rootScope.user.isAdmin){
        $rootScope.setupPusher();
     }

}]);
