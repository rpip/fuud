from .models import User


class FuudMiddleware(object):
    """Application-wide middleware.

    This middleware is responsible for setting/toggling
    some clientside variables via cookies.

    Sets/toggles the following cookies:
    fuud_username
    fuud_phone_number
    fuud_email
    fuud_address
    fuud_is_authenticated
    fuud_is_staff
    fuud_is_admin
    fuud_subscribed_to_updates
    """
    def process_response(self, request, response):
        if hasattr(request, 'user'):
            if request.user.is_authenticated() and not request.COOKIES.get('fuud_isAuthenticated'):
                user = User.objects.filter(email=request.user.email)[0]
                response.set_cookie('fuud_username', user.username)
                response.set_cookie('fuud_phone_number', user.userprofile.phone_number)
                response.set_cookie('fuud_email', str(user.email)),
                response.set_cookie('fuud_subscribed_to_updates',
                                    to_int(user.userprofile.subscribed_to_updates))
                response.set_cookie('fuud_address', str(user.userprofile.address))
                response.set_cookie('fuud_is_admin', to_int(user.is_superuser))
                response.set_cookie('fuud_is_staff', to_int(user.is_staff))
                response.set_cookie('fuud_is_authenticated', 1)

        return response


def to_int(is_true):
    '''Returns 1 for True

    Likewise, returns 0 for False
    '''
    if is_true:
        return 1
    return 0
