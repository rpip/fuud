import os
from django.core.management.base import BaseCommand
from django.conf import settings
from optparse import make_option
from fuud.utils import parse_config_file
import datetime


class Command(BaseCommand):
    """Parses applications/services configuration files.

    This command parses the configuration files for 3rd party services/apps
    such as NGinX, and Supervisord. The results are then written to a path.
    The out put directory defaults to './_output' in this command's directory

    """
    can_import_settings = True

    option_list = BaseCommand.option_list + (
        make_option('--output',
                    action='store',
                    dest='output_path',
                    help='Destination of the updated MANIFEST'),
        make_option('--input',
                    action='store',
                    dest='input_path',
                    help='Path to read/get the MANIFEST'),
    )

    def handle(self, *args, **options):
        input_path = settings.MANIFEST_TPL_PATH
        output_path = settings.MANIFEST_FILE_PATH

        if options['input_path'] and os.path.exists(options['input_path']):
            input_path = options['input_path']

        if options['output_path']:
            output_path = options['output_path']

        if not os.path.exists(os.path.dirname(output_path)):
            self.stdout.write('** Creating output directory: %s' % output_path)
            os.makedirs(os.path.dirname(output_path))

        output = parse_config_file(input_path, {
            'timestamp': str(datetime.datetime.now())
        })
        with open(output_path, 'w') as fp:
            print self.stdout.write("Writing Application cache \
            MANIFEST to %s \n" % output_path)
            fp.write(output)
