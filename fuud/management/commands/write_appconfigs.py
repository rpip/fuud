import os
import glob
from django.core.management.base import BaseCommand
from django.conf import settings
from optparse import make_option
from fuud.utils import parse_config_file


class Command(BaseCommand):
    """Parses applications/services configuration files.

    This command parses the configuration files for 3rd party services/apps
    such as NGinX, and Supervisord. The results are then written to a path.
    The out put directory defaults to './_output' in this command's directory

    """
    can_import_settings = True

    option_list = BaseCommand.option_list + (
        make_option('--output',
                    action='store',
                    dest='output_dir',
                    help='Destination of the parsed application/services config files'),
        make_option('--input',
                    action='store',
                    dest='input_dir',
                    help='Path to read/get the application/services config files'),
    )

    def handle(self, *args, **options):
        PATTERN = '*.conf'
        input_dir = os.path.join(settings.CONFIG_FILES_INPUTDIR, PATTERN)
        output_dir = settings.CONFIG_FILES_OUTPUTDIR

        if options['input_dir'] and os.path.exists(options['input_dir']):
            input_dir = os.path.join(options['input_dir'], PATTERN)

        if options['output_dir']:
            output_dir = options['output_dir']

        if not os.path.exists(output_dir):
            self.stdout.write('** Creating output directory: %s' % output_dir)
            os.makedirs(output_dir)

        files = glob.glob(input_dir)
        if not files:
            return self.stdout.write("No configurations found matching %s \n"
                                     % input_dir)

        # parse and write files
        for filepath in files:
            filename = os.path.basename(filepath)
            self.stdout.write('** Parsing: %s \n' % filename)
            parsed_template = parse_config_file(filepath,
                                                 settings.CONFIG_FILES_CONTEXT)
            with open(os.path.join(output_dir, filename), 'w') as output_filepath:
                self.stdout.write('** Writing %s to %s \n' %
                                  (filename, output_filepath.name))
                output_filepath.write(parsed_template)
