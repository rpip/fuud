import os
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from optparse import make_option
from fuud.utils.menu import import_food_menu


class Command(BaseCommand):
    """Import restaurant menu items from file into database """
    can_import_settings = True

    option_list = BaseCommand.option_list + (
        make_option('--file',
                    action='store',
                    dest='file',
                    help='Loads restaurant menu items from a \
                    file into the database'),
        )

    def handle(self, *args, **options):
        if  options['file']:
            filepath = os.path.abspath(options['file'])
            if os.path.exists(filepath):
                self.stdout.write("*** Importing menu items from %s \n" % filepath)
                return import_food_menu(filepath)
            else:
                raise CommandError("Restaurant menu file not found: %s" % filepath)

        self.stdout.write("Importing default menu file: %s \n" %
                          settings.DEFAULT_RESTAURANT_MENU_FILE)
        # import the default food menu file
        import_food_menu(settings.DEFAULT_RESTAURANT_MENU_FILE)
