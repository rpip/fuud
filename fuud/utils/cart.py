from fuud.models import Item, OrderedItem, Order
from apps.api.serializers import ItemSerializer
from . import redis_set_obj
from . import make_random_id


class Cart(object):
    """A session-based cart for orders

    Cart = [CartLine()]
    """
    def __init__(self, **kwargs):
        self._items = []
        self._user = kwargs.get('user')
        self._key = 'fuud:cart:%s' % make_random_id()

    def add(self, item, qty=1):
        'Add an item/items to the cart'
        if isinstance(item, CartLine):
            return self._items.append(item)
        return self._items.append(CartLine(item, qty))

    @property
    def items(self):
        'Returns the items in the cart'
        return self._items

    def remove(self, itemid):
        'Removes an item from the cart'
        self._items = [x for x in self._items if itemid != x.item]

    def clear(self):
        """Clear all items in the cart"""
        self._items = []

    def update(self, itemid, qty):
        'Update the quantity of an item in the cart'
        item = [x for x in self._items if itemid == x.item]
        if item:
            _items = [x for x in self._items if itemid != x.item]
            _items.append(CartLine(itemid, qty))
            self._items = _items
            return True
        else:
            raise ValueError("No item found with id: {}".format(itemid))

    def _get_queryset(self):
        'Returns a queryset object from the items in the cart'
        queryset = Item.objects.filter(pk__in=[x.item for x in self._items])
        return queryset

    def serialize(self):
        'Returns serialized data representation of the items in the cart'
        serialized = ItemSerializer(self._get_queryset(), many=True)
        return serialized

    @property
    def total(self):
        'Returns the total cost of the items in the cart'
        return sum([x.total for x in self._items])

    def save_to_redis(self):
        'Save cart items to Redis'
        return redis_set_obj(self._key, self._items)

    def count(self, by_qty=False):
        '''Returns the number of items in the cart

        Supports an optional "by_qty" argumnent for \
        summing the quantity of each item
        '''
        if by_qty:
            return sum([x.qty for x in self._items])
        return len(self._items)

    def save_as_order(self, user=None):
        '''Persist the cart as an order in the database'''
        if user:
            self._user = user
        order = Order.objects.create(user=self._user)
        order.save()
        for line in self.items:
            item = OrderedItem.objects.create(
                item_id=line.item,
                quantity=line.qty
            )
            order.items.add(item)
        order.save()
        return order

class CartLine(object):
    "Class to represent a single item in a cart"
    def __init__(self, itemid=None, qty=0):
        self.item = int(itemid)
        self.qty = int(qty)

    def as_obj(self):
        try:
            item = Item.objects.get(pk=self.item)
            return item
        except Item.DoesNotExist:
            return None

    @property
    def total(self):
        item = self.as_obj()
        if item:
            return item.total_price * self.qty
        return 0

    def __str__(self):
        print "item: {} \n qty: {}".format(self.product, self.qty)
