import os
import re
from django.template.defaultfilters import slugify
from django.conf import settings
from fuud.models import Menu, Item, ItemCategory, ItemImage


def parse_restaurant_menu(filepath=None):
    """Parse and return a JSON representation
    of  a restaurant menu text file.

    Menu file format:
      #drinks
      Red Wine GHC 20.00
      Juice USD 10.99

      #meat
      Goat meat GHC 30.89
    # fixme: improve parser with pyparsing
    """
    # regex to match a menu item: Goat meat GHC 30.89
    regex = re.compile(r"""
      \n*(?P<item>.*?)\t*\n*\s*(?P<currency>[A-Z]{3})\t*\s*(?P<cost>\d+(\.\d{1,2})?)\t*\s*
      """, re.VERBOSE)

    if filepath:
        fp = open(os.path.abspath(filepath))
    else:
        fp = open(settings.DEFAULT_RESTAURANT_MENU_FILE)

    menu = {}
    data = fp.read().split('#')[1:]
    menu_subs = [sub_menu.split('\n') for sub_menu in data]

    for menu_category in menu_subs:
        category, category_items = (menu_category[0], menu_category[1:])

        for item in category_items:
            match = regex.match(item)
            if not match:
                continue
            item_dict = match.groupdict()
            if category not in menu:
                # menu[category] = [item()]
                menu[category] = [item_dict]
            else:
                menu[category].append(item_dict)
    return menu


def import_food_menu(filepath):
    """Parses and imports menu items from a file into the DB

    Filepath must be a valid file path with restaurant menu items
    or it defaults to the DEFAULT_RESTAURANT_MENU_FILE in the settings
    """
    menu = parse_restaurant_menu(filepath)
    for category in menu:
        save_menu_items(category, menu[category])


def save_menu_items(menu_category, menu_category_items):
    """Save a menu dictionary into the database"""
    (mainmenu, created) = Menu.objects.get_or_create(name='Default Menu')
    (category_obj, _) = ItemCategory.objects.get_or_create(name=menu_category,
                                                        slug=slugify(menu_category))

    default_image,_ = ItemImage.objects.get_or_create(is_cover=True,
                                                      image_file=settings.DEFAULT_ITEM_IMAGE_PATH)

    for item in menu_category_items:
        print "*** saving item: %s" % item
        (itemobj, _) = Item.objects.get_or_create(price=item['cost'],
                                                  name=item['item'],
                                                  description="",
                                                  slug=slugify(item['item']),
                                                  category=category_obj)
        itemobj.images.add(default_image)
        itemobj.save()
