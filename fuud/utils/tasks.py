"""Tasks

Delayed tasks
"""
from fuud.utils.cart import Cart


def save_order(user, items):
    '''Save an order'''
    cart = Cart(user=user)
    for item, qty in items.items():
        cart.add(int(item), int(qty))

    cart.save_as_order()
