"""Utility/miscellaneuos functions/helpers"""
import uuid
import os
import pickle
from django.core.cache import cache
from django.contrib.auth.models import User
from django.template import Template, Context
from django.conf import settings
import pusher


def generate_order_ref():
    """Returns a unique reference uuid

    The generate reference id/key is an
    alpha-numeric character set.
    Example: 595f8a27-4bbc-49db-8e84-28d93400a37e
    """
    return uuid.uuid4().hex


def make_random_id():
    return os.urandom(64/8).encode('hex_codec')


def redis_set_obj(key, obj):
    'Saves a Python object in Redis'
    return cache.set(key, pickle.dumps(obj))


def redis_get_obj(key):
    "Returns the object 'as stored' in  Redis"
    pickled_value = cache.get(key)
    if pickled_value is None:
        return None
    return pickle.loads(pickled_value)


def create_dummy_user():
    '''Creates a dummy user object

    A use case is during unit testing
    '''
    user, _ = User.objects.get_or_create(email='mawuli@foo.com',
                                            password='abc',
                                            username='foobar')
    return user


def parse_config_file(filepath, context={}):
    if not os.path.exists(filepath):
        raise ValueError("Template file not found: %s" % filepath)

    template = Template(open(filepath).read())
    return template.render(Context(context))


def init_pusher(**kwargs):
    app_id = kwargs.get('app_id', settings.PUSHER_APPID)
    key = kwargs.get('key', settings.PUSHER_KEY)
    secret = kwargs.get('secret', settings.PUSHER_SECRET)
    return pusher.Pusher(
        app_id=app_id,
        key=key,
        secret=secret
    )


def internationalize_phone(phone):
    '''Formats local number to international standard/format

    Example: 0266636984 -> +233266636984
    '''
    international = phone.startswith('+233') \
                                  or phone.startswith('00233')
    if not international:
        return '+233' + phone
    return phone
