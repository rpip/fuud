from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'fuud.views.home', name='home'),
    url(r'^login-error/', 'fuud.views.login_error', name='login_error'),
    url(r'^logout/', 'fuud.views.logout_user', name='logout'),
    url(r'^api/', include('apps.api.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social')),
)


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# queue
urlpatterns += patterns('',
    (r'^django-rq/', include('django_rq.urls')),
)

# django debug toolbar
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
