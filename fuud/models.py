# models
from django.db import models
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete
from django.utils.translation import ugettext_lazy as _
from django_prices.models import PriceField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
from phonenumber_field.modelfields import PhoneNumberField
from fuud.utils import generate_order_ref
from .signals import (
    new_order_hook,
    delete_order_hook,
    create_user_profile
)


# Storage class
image_storage = FileSystemStorage()


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    address = models.TextField(null=True, blank=True)
    phone_number = PhoneNumberField(null=True, blank=True)
    subscribed_to_updates = models.BooleanField(default=True);
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Tax(models.Model):
    """Taxes on menu items

    Examples: VAT, NHIS, VRA etc
    """
    name = models.CharField(max_length=30)
    value = models.FloatField()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Taxes')
        verbose_name_plural = _('Taxes')

    def __unicode__(self):
        return self.name


class ItemImage(models.Model):
    """Images for the menu items"""
    image_file = models.ImageField(upload_to='item_images',
                                   default=settings.DEFAULT_ITEM_IMAGE_PATH)
    thumbnail = ImageSpecField(source='image_file',
                               processors=[ResizeToFill(100, 50)],
                               format='JPEG',
                               options={'quality': settings.THUMBNAIL_QUALITY})

    is_cover = models.BooleanField(default=False)

    @property
    def thumbnail_url(self):
        return self.thumbnail.url

    def __unicode__(self):
        return self.thumbnail.url


class ItemCategory(models.Model):
    """Menu item categories"""
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True, null=True, blank=True)
    taxes = models.ManyToManyField(Tax, null=True, blank=True)
    image = models.ForeignKey(ItemImage, null=True, blank=True)

    @property
    def items(self):
        return self.item_set.all()

    @property
    def thumbnail_url(self):
        if self.image:
            return self.image.thumbnail_url
        else:
            return settings.DEFAULT_ITEM_IMAGE_PATH

    def __unicode__(self):
        return self.name


class Item(models.Model):
    """Menu/food item model"""
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True, null=True, blank=True, max_length=300)
    description = models.CharField(max_length=255, null=True, blank=True)
    category = models.ForeignKey(ItemCategory)
    images = models.ManyToManyField(ItemImage, null=True, blank=True)
    price = PriceField('Price',
                       currency=settings.DEFAULT_ITEM_CURRENCY,
                       max_digits=12,
                       decimal_places=4)
    # keep track of the item stock
    stockrecord = models.IntegerField(default=settings.DEFAULT_ITEM_STOCK)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.slug)
            super(Item, self).save(*args, **kwargs)

    @property
    def cover_image(self):
        try:
            return self.images.filter(is_cover=True)[0].thumbnail_url
        except IndexError:
            if self.images.count():
                return self.images.all()[0].thumbnail_url
            else:
                return settings.DEFAULT_ITEM_IMAGE_PATH

    @property
    def item_images(self):
        return [img.thumbnail_url for img in self.images.all()]

    @property
    def taxes(self):
        """Returns the sum of all taxes on this items"""
        return sum([tax.value for tax in self.category.taxes.all()])

    @property
    def total_price(self, raw=True):
        if raw:
            return (self.taxes + float(self.price.gross))
        return self.price.currency + ' ' + \
            str(self.taxes + float(self.price.gross))

    @property
    def in_stock(self):
        """Returns True if there are more items in stock"""
        return self.stockrecord > 0

    def __unicode__(self):
        return self.name


class Menu(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True, null=True, blank=True)
    items = models.ManyToManyField(Item, null=True, blank=True)

    def menu_items(self):
        return ', '.join([i.name for i in self.items.all()])

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.slug)
            super(Menu, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name


class Order(models.Model):
    """Holds information about a single order"""
    ref = models.CharField(max_length=100, unique=True)
    user = models.ForeignKey(User)
    comment = models.TextField(null=True, blank=True)  # admin's comment
    note = models.CharField(max_length=100, null=True, blank=True)  # user's comment
    PENDING = 0
    PROCESSED = 1
    CANCELLED = 2
    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (PROCESSED, 'Processed'),
        (CANCELLED, 'Cancelled')
    )
    status = models.IntegerField(choices=STATUS_CHOICES,
                                 default=PENDING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def total_cost(self):
        return settings.DEFAULT_ITEM_CURRENCY + ' ' + \
            str(sum([float(i.subtotal) for i in self.items.all()]))

    @property
    def number_of_items(self):
        return self.items.count()

    def save(self, *args, **kwargs):
        """Automatically populate 'reference' field"""
        if not self.id:
            self.ref = generate_order_ref()
            super(Order, self).save(*args, **kwargs)


    def __unicode__(self):
        return self.ref


class OrderedItem(models.Model):
    """
    Stores information about a single item conbination for an order
    """
    item = models.ForeignKey(Item)
    quantity = models.IntegerField(default=1)
    order = models.ForeignKey(Order, related_name='items',
                              null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.item.name

    @property
    def subtotal(self):
        return float(self.quantity * self.item.total_price)


class Feedback(models.Model):
    """Feedback from customers"""
    subject = models.CharField(max_length=100)
    message = models.TextField(max_length=500)
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    PENDING = 0
    PROCESSED = 1
    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (PROCESSED, 'Processed'),
    )
    status = models.IntegerField(choices=STATUS_CHOICES,
                                 default=PENDING)

    def __unicode__(self):
        return self.subject


# register hooks
post_save.connect(new_order_hook, sender=Order)
post_delete.connect(delete_order_hook, sender=Order)
post_save.connect(create_user_profile, sender=User)
