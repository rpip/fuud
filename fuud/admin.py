from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from imagekit.admin import AdminThumbnail
from .models import (Item, ItemImage, ItemCategory, UserProfile,
                     Order, OrderedItem, Menu, Tax, Feedback)


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'profile'


class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )


class ItemAdmin(admin.ModelAdmin):
    model = Item
    list_display = ('name', 'total_price', 'stockrecord', 'category')
    list_filter = ('category',)
    prepopulated_fields = {'slug': ('name',)}


class ItemInline(admin.StackedInline):
    model = Item
    can_delete = True
    verbose_name_plural = 'items'


class ItemCategoryAdmin(admin.ModelAdmin):
    model = ItemCategory
    inlines = (ItemInline,)
    list_display = ('name', 'image', 'items')
    prepopulated_fields = {'slug': ('name',)}

    def items(self, obj):
        return obj.items.count()


class ItemImageAdmin(admin.ModelAdmin):
    model = ItemImage
    list_display = ('__str__', 'admin_thumbnail')
    admin_thumbnail = AdminThumbnail(image_field='thumbnail')


class ItemImagesAdmin(admin.ModelAdmin):
    model = ItemImage


class OrderedItemAdmin(admin.ModelAdmin):
    model = OrderedItem
    list_filter = ('item__category',)
    list_display = ('item', 'quantity', 'order',
                    'created_at', 'updated_at')


class OrderedItemInline(admin.TabularInline):
    model = OrderedItem
    can_delete = True
    verbose_name_plural = 'items'


class OrderAdmin(admin.ModelAdmin):
    model = Order
    inlines = (OrderedItemInline,)
    list_display = ('ref', 'user', 'status', 'number_of_items',
                    'total_cost', 'created_at', 'updated_at')


class MenuAdmin(admin.ModelAdmin):
    model = Menu
    list_display = ('name', 'menu_items')


class TaxAdmin(admin.ModelAdmin):
    model = Tax


class FeedbackAdmin(admin.ModelAdmin):
    model = Feedback
    list_display = ('subject', 'date', 'username', 'status',)

    def date(self, obj):
        return obj.created_at

    def username(self, obj):
        return obj.user.username

# Re-register admin models
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Menu, MenuAdmin)
admin.site.register(Tax, TaxAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(ItemCategory, ItemCategoryAdmin)
admin.site.register(ItemImage, ItemImageAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderedItem, OrderedItemAdmin)
admin.site.register(Feedback, FeedbackAdmin)
