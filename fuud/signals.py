from django.db.models import get_model
from django.core.mail import send_mail
from django.conf import settings


## Hooks
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile = get_model('fuud','UserProfile')
        UserProfile.objects.create(user=instance)


def new_order_hook(sender, instance, created, **kwargs):
    if created:
        subject = 'New Order: %s' % settings.APP_NAME
        message = '''
        Reference: %s.
        Total: %s
        Number of items: %s
        Note: %s
        ''' % (instance.ref, instance.total_cost,
               instance.number_of_items, instance.note)
        send_mail(subject, message, settings.EMAIL_HOST_USER,
                  settings.EMAIL_ON_ORDERS, fail_silently=False)


## Hooks
def delete_order_hook(sender, instance, **kwargs):
    subject = 'Deleted Order: %s' % settings.APP_NAME
    message = "Reference: %s" % instance.ref
    send_mail(subject, message, settings.EMAIL_HOST_USER,
              settings.EMAIL_ON_ORDERS, fail_silently=False)
