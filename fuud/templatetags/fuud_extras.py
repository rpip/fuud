"""fuud_extras

Custom template tags for the application
"""
import os
import glob
import datetime
from django import template
from django.conf import settings


register = template.Library();


@register.simple_tag
def settings_value(key):
    'Returns the value of the key app settings'
    return getattr(settings, key, "")


@register.simple_tag
def angular_template(*files):
    'Returns content of AngularJS templates'
    ANGULAR_TPL = """
    <script type="text/ng-template" id="{id}">
    {body}
    </script>"""
    templates = []
    rootpath = os.path.abspath(settings.ANGULAR_TEMPLATES_DIR)
    if files[0] == '*':
        files = glob.glob(rootpath + '/*' +
                          settings.ANGULAR_TEMPLATE_EXT)
    else:
        files = map(lambda f: os.path.join(rootpath, f), files)
        files = filter(lambda f: os.path.exists(f), files)
    for filepath in files:
        filename = os.path.basename(filepath)
        if filename not in settings.ANGULAR_TEMPLATE_EXCLUDE:
            with open(filepath) as fh:
                template = ANGULAR_TPL.format(
                    id=filename,
                    body=fh.read()
                )
                templates.append(template)
    templates = '\n\n'.join(templates)
    templates = '''
    <!-- AngularJS templates: {time}-->
    {templates}
    <!--//AngularJS templates: {time}-->
    '''.format(time=datetime.datetime.now(),
               templates=templates
               )
    return templates


class VerbatimNode(template.Node):
    """Verbatim template tag
      jQuery templates use constructs like:
      {{if condition}} print something{{/if}}

        Or like:

      {% if condition %} print {%=object.something %}{% endif %}

    This, of course, completely screws up Django templates,
    because Django thinks {{ and }} mean something.

    Wrap {% verbatim %} and {% endverbatim %} around those
    blocks of jQuery templates and this will try its best
    to output the contents with no changes.

    ## source: https://gist.github.com/andrusha/2409775
    """
    def __init__(self, text):
        self.text = text

    def render(self, context):
        return self.text


@register.tag
def verbatim(parser, token):
    text = []
    while 1:
        token = parser.tokens.pop(0)
        if token.contents == 'endverbatim':
            break
        if token.token_type == template.TOKEN_VAR:
            text.append('{{ ')
        elif token.token_type == template.TOKEN_BLOCK:
            text.append('{%')
        text.append(token.contents)
        if token.token_type == template.TOKEN_VAR:
            text.append(' }}')
        elif token.token_type == template.TOKEN_BLOCK:
            if not text[-1].startswith('='):
                text[-1:-1] = [' ']
            text.append(' %}')
    return VerbatimNode(''.join(text))
