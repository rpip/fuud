from django.test import TestCase
from fuud.models import OrderedItem, Order, Item, ItemCategory
from fuud.utils.cart import Cart, CartLine
from fuud.utils import (
    generate_order_ref,
    create_dummy_user,
    make_random_id,
)


class CartTest(TestCase):
    fixtures = ['fuud_testdata.json']

    def setUp(self):
        # items to use for testing
        self.items = [x.pk for x in Item.objects.all()[:6]]
        dummy_user = create_dummy_user()
        self.cart = Cart(user=dummy_user)
        item = CartLine(self.items[5], 7)
        self.cart.add(item)

    def tearDown(self):
        self.cart = None

    def test_add_to_cart(self):
        item1 = CartLine(self.items[0], 3)
        item2 = CartLine(self.items[1], 8)
        item3 = CartLine(self.items[2], 7)
        self.cart.add(item1)
        self.cart.add(item2)
        self.cart.add(item3)
        # Alternative ways of adding items to cart
        self.cart.add(self.items[3])
        self.cart.add(self.items[4], 3)
        # count the number of items in cart
        self.assertTrue(self.cart.count() == 6)

    def test_count_cart_items(self):
        # count the number of items in cart
        self.assertTrue(self.cart.count() == 1)
        # sum quantity of items in cart
        self.assertTrue(self.cart.count(True) == 7)

    def test_update_cart(self):
        # reduce the quantity of item1 by '2'
        self.cart.update(self.items[5], 2)
        # sum quantity of items in cart
        self.assertTrue(self.cart.count(True) == 2)


class ItemTest(TestCase):
    fixtures = ['fuud_testdata.json']

    def setUp(self):
        self.user = create_dummy_user()
        self.item_category = ItemCategory.objects.all()[0]
        self.item, _ = Item.objects.get_or_create(name="Coconut Drink",
                                                  description='foo',
                                                  category=self.item_category,
                                                  price=20,
                                                  slug='coconut-drink')

    def tearDown(self):
        self.user = None

    def test_validate_item_basic_props(self):
        self.assertEqual(self.item.category, self.item_category)
        # an empty list evaluates to False
        self.assertFalse(self.item.images.all())
        self.assertTrue(self.item.total_price > 0)

    def test_item_stockrecord(self):
        self.assertTrue(self.item.in_stock)
        self.item.stockrecord = 0
        self.assertFalse(self.item.in_stock)


class OrdersTest(TestCase):
    fixtures = ['fuud_testdata.json']

    def setUp(self):
        self.order_ref = generate_order_ref()
        self.user = create_dummy_user()
        self.order, _ = Order.objects.get_or_create(user=self.user,
                                                 ref=self.order_ref)

    def tearDown(self):
        self.user.delete()
        self.order.delete()

    def test_count_order_items(self):
        item1 = OrderedItem(item_id=1, quantity=3)
        item2 = OrderedItem(item_id=5, quantity=6)
        item3 = OrderedItem(item_id=12, quantity=1)
        self.order.items.add(item1)
        self.order.items.add(item2)
        self.order.items.add(item3)
        self.assertTrue(self.order.items.count() == 3)

    def test_order_total(self):
        self.assertTrue(self.order.total_cost > 0)

    def test_unique_order_refs(self):
        ref1 = generate_order_ref()
        ref2 = generate_order_ref()
        self.assertNotEqual(ref1, ref2)

    def test_order_status(self):
        self.assertTrue(self.order.status == Order.PENDING)
        self.order.status = Order.PROCESSED
        self.assertTrue(self.order.status == Order.PROCESSED)


class GenralTest(TestCase):
    def test_unique_random_numbers(self):
        token1 = make_random_id()
        token2 = make_random_id()
        self.assertNotEqual(token1, token2)


class TestAPIEndpoints(TestCase):
    def test_endpoint_exists(self):
        response = self.client.get('/api/items/')
        self.assertEqual(response.status_code, 200)

    def test_endpoint_does_not_exists(self):
        response = self.client.get('/api/nothing_here/')
        self.assertEqual(response.status_code, 404)
