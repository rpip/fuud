from django.shortcuts import render, redirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import logout


def home(request):
    return render_to_response('home.html', {
        'HOST': request.get_host()
        }, RequestContext(request))


def login_error(request):
    'Page to display when login fails'
    return render(request, "login_error.html")


def logout_user(request):
    'Log user out of the current session'
    logout(request)
    return redirect('/')


def error404(request):
    return render(request,'404.html')
