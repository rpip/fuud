import datetime
from django.contrib.auth.models import User
from rest_framework import viewsets, status, filters
from rest_framework.response import Response
from rest_framework.decorators import action, api_view, link
from rest_framework.status import (
    HTTP_401_UNAUTHORIZED, HTTP_500_INTERNAL_SERVER_ERROR,
    HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN
)
import django_rq
from django.conf import settings
from fuud.models import (
    Item, ItemCategory,
    ItemImage, Order,
    UserProfile, Feedback
)
from .serializers import (ItemSerializer, ItemImageSerializer,
                          OrderSerializer, UserProfileSerializer,
                          ItemCategorySerializer, FeedbackSerializer)
from fuud.utils import init_pusher, internationalize_phone
from fuud.utils.cart import Cart
from fuud.utils.tasks import save_order


class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows items to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

    def list(self, request):
        serializer = ItemSerializer(self.queryset, many=True)
        return Response({
            'data': serializer.data,
            'success': True
        })


class ItemCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows categories to be viewed or edited.
    """
    queryset = ItemCategory.objects.all()
    serializer_class = ItemCategorySerializer

    def list(self, request):
        serializer = ItemCategorySerializer(self.queryset, many=True)
        return Response({'categories': serializer.data})


class ItemImageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows item images to be viewed or edited.
    """
    queryset = ItemImage.objects.all()
    serializer_class = ItemImageSerializer


class OrderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows orders to be viewed or edited.
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = (filters.DjangoFilterBackend,)

    def list(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        return Response({
            'data': serializer.data,
            'success': True
        })

    def create(self, request):
        pass

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass

    @link()
    def orders(self, request, pk):
        '''Returns orders placed by a particular user.

        api/orders/<username>/orders

        '''
        orders = self.queryset.filter(user__username=pk)
        if orders:
            serializer = self.serializer_class(orders, many=True)
            return Response(serializer.data)
        else:
            return Response({
                'success': 1,
                'data': []
            })

    @link()
    def update_status(self, request, pk):
        '''Update order status

        api/orders/<reference>/update_status

        '''
        orders = self.queryset.filter(ref=pk, user=request.user)
        if orders:
            order = orders[0]
            new_status = request.GET.get('status')
            order.status = int(new_status)
            order.save()
            return Response({'success':1})
        else:
            return Response({
                'success': 0,
                'message': 'Order: {ref} not found'.format(ref=pk)
            })

    @action()
    def submit(self, request):
        'Save the order in the database'
        pass


@api_view(['POST'])
def checkout(request):
    'Checkout items in the cart'
    try:
        username = request.DATA.get('username')
        items = request.DATA.get('items')
        user = User.objects.get(username=username)
        if user:
            # process large orders asynchronously
            if sum(items.values()) > settings.ASYNC_CART_LIMIT:
                result = django_rq.enqueue(save_order, user, items)
                p = init_pusher()
                message = '''
                A new order of {count} items has been made by {username}!
                Date: {date}
                Please check the dashboard for order details.
                '''.format(
                    count=sum(items.values()),
                    username=user.username,
                    date=str(datetime.datetime.utcnow())
                )
                p['orders'].trigger('new_order', {'message': message})
                return Response({
                    'success': True
                })
            else:
                cart = Cart(user=user)
                for item, qty in items.items():
                    cart.add(int(item), int(qty))

                order = cart.save_as_order()
                serializer = OrderSerializer(order)
                message = '''
                A new order of {count} items has been made by {username}!
                Total: {total}
                Reference: {ref}
                Date: {date}
                Please check the dashboard for order details.
                '''.format(
                    count=sum(items.values()),
                    username=user.username,
                    ref=order.ref,
                    total=order.total_cost,
                    date=str(datetime.datetime.utcnow())
                )
                p = init_pusher()
                p['orders'].trigger('new_order', {'message': message})
                return Response({
                    'data': serializer.data,
                    'success': True
                })
        else:
            return Response({
                'message': "Invalid data or missing params",
                'success': False
            }, status=HTTP_401_UNAUTHORIZED)
    except Exception, err:
        print "order exception: %s" % err
        return Response({
            'message': "Invalid data or missing params",
            'success': False
        }, status=HTTP_500_INTERNAL_SERVER_ERROR)


class UserProfileViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed
    """
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    @action()
    def update_info(self, request, pk):
        '''Update user info

        api/<username>/update_info/
        '''
        username = pk
        user = User.objects.get(username=username)
        if user:
            email = request.DATA.get('email')
            phone = request.DATA.get('phone')
            address = request.DATA.get('address')
            fields = (email, phone, address)
            if all(fields):
                user.email = email
                user.userprofile.address = address
                user.userprofile.phone_number = internationalize_phone(phone)
                user.save()
                user.userprofile.save()

            # subscription to updates
            subscribed = request.DATA.get('subscribed')
            if subscribed:
                user.userprofile.subscribed_to_updates = True
                user.userprofile.save()

            return Response({
                'success':1
            })
        else:
            return Response({
                'message': "Username %s not found" % username,
                'success': False
            }, status=HTTP_400_BAD_REQUEST)


class FeedbackViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows for feedbacks
    """
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer

    def list(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        return Response({
            'data': serializer.data,
            'success': True
        })

    def create(self, request):
        'Submit a feedback'
        try:
            username = request.DATA.get('username')
            subject = request.DATA.get('subject')
            message = request.DATA.get('message')
            user = User.objects.filter(username=username)
            if user:
                feedback = Feedback.objects.create(
                    user=user,
                    message=message,
                    subject=subject
                )
                feedback.save()
                serializer = FeedbackSerializer(feedback)
                return Response({
                    'data': serializer.data,
                    'success': True
                })
            else:
                return Response({
                    'message': "Invalid data or missing params",
                    'success': False
                }, status=HTTP_401_UNAUTHORIZED)
        except:
            return Response({
                'message': "Invalid data or missing params",
                'success': False
            }, status=HTTP_500_INTERNAL_SERVER_ERROR)

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass
