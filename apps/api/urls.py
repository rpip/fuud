from django.conf.urls import patterns, url, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import routers
import views

router = routers.DefaultRouter()
router.register(r'items', views.ItemViewSet)
router.register(r'orders', views.OrderViewSet)
router.register(r'feedbacks', views.FeedbackViewSet)
router.register(r'users', views.UserProfileViewSet)
router.register(r'images', views.ItemImageViewSet)
router.register(r'categories', views.ItemCategoryViewSet)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browseable API.
urlpatterns = patterns('',
    url(r'^orders/checkout/?', views.checkout),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)


#urlpatterns = format_suffix_patterns(urlpatterns)
