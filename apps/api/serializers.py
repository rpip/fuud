"""REST API data serializers

Responsible for serializing Django model instances into
 the right format.
"""
from django.contrib.auth.models import User
from rest_framework import serializers
from fuud.models import (
    Item, ItemImage, ItemCategory,
    Order, OrderedItem, Menu, Tax,
    Feedback
)

from fuud.models import UserProfile


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email')


class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = UserProfile
        fields = ('address', 'user')


class TaxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tax
        fields = ('name', 'value',)


class ItemImageSerializer(serializers.ModelSerializer):
    thumbnail_url = serializers.Field()

    class Meta:
        model = ItemImage
        fields = ('thumbnail_url',)


class ItemSerializer(serializers.ModelSerializer):
    cover_image = serializers.Field()
    images = ItemImageSerializer()
    taxes = serializers.Field()
    price = serializers.Field(source='total_price')
    leaf = serializers.SerializerMethodField('is_leaf')
    stock = serializers.Field(source='stockrecord')

    class Meta:
        model = Item
        fields = ('id', 'name', 'slug', 'description', 'category',
                  'stock', 'images', 'taxes', 'price',
                  'cover_image', 'leaf',)

    def is_leaf(self, obj):
        return True


class ItemCategorySerializer(serializers.ModelSerializer):
    taxes = TaxSerializer(many=True)
    items = ItemSerializer()
    leaf = serializers.SerializerMethodField('is_leaf')
    image = serializers.RelatedField(source='thumbnail_url')

    class Meta:
        model = ItemCategory
        fields = ('id', 'name', 'slug', 'image', 'taxes', 'items', 'leaf',)

    def is_leaf(self, obj):
        return True

class MenuSerializer(serializers.ModelSerializer):
    items = ItemSerializer(many=True)

    class Meta:
        model = Menu
        fields = ('name', 'slug', 'items',)


class OrderedItemSerializer(serializers.ModelSerializer):
    item = ItemSerializer()
    user = UserProfileSerializer()
    subtotal = serializers.Field(source='subtotal')

    class Meta:
        model = OrderedItem
        fields = ('item', 'quantity', 'subtotal')


class OrderSerializer(serializers.ModelSerializer):
    items = OrderedItemSerializer(many=True)
    user = UserSerializer()
    total_cost = serializers.Field(source='total_cost')
    status = serializers.Field(source='status')

    class Meta:
        model = Order
        fields = ('ref', 'total_cost', 'status', 'created_at',
                  'updated_at', 'user', 'items',)


class FeedbackSerializer(serializers.ModelSerializer):
    user = UserProfileSerializer()
    message = serializers.Field(source='message')
    created_at = serializers.Field(source='created_at')
    status = serializers.Field(source='status')

    class Meta:
        model = Feedback
        fields = ('user', 'message', 'created_at', 'status',)
