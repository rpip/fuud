Fuud
====

A Django and AngularJS Restaurant menu ordering app.

#Settings
The manage.py script checks for an environment variable called `ENVIRONMENT` with values of STAGING or PRODUCTION. If none of those are found, development environment is assumed. We use this to automatically set `DJANGO_SETTINGS_MODULE`. This makes using manage.py easier because you dont have to keep remembering of writing `--setting=project.settings.development`. We converted settings.py into a module with settings splitted into three different files, `development.py`, `staging.py`, `production.py`. All of those import common settings from `common.py` and override them or set new ones depending on the environment.

To setup your local environment you should create a virtualenv and install the necessary requirements:

    mkvirtualenv --distribute {{ project_name }}
    $VIRTUAL_ENV/bin/pip install -r $PWD/requirements/dev.txt
    echo "export ENVIRONMENT=DEVELOPMENT" >> $VIRTUAL_ENV/bin/postactivate
    echo "unset ENVIRONMENT" >> $VIRTUAL_ENV/bin/postdeactivate


Exit the virtualenv and reactivate it to activate the settings just changed:

    deactivate
    workon {{ project_name }}

#Setup

##Install requirements
    pip install -r requirements.txt
    # Install Redis and make sure it is running on.
    # Export the url to your Redis instance; defaults to localhost:6379
    export REDISTOGO_URL='redis://localhost:6379'

##Database
    # export the url of your preferred database
    export DATABASE_URL="DATABASE URL HERE"
    ./manage.py syncdb --all

##Import menu items
    ./manage.py import_menu

##Collect static files
    # this should be enough for development
    # For production, host these files on a separate server
    ./manage.py collectstatic


##Run
     ./manage.py runserver
     # OR to use gunicorn, type:
     ./manage.py run_gunicorn -b "0.0.0.0:8000" -w 3

      # To run the application in full production mode,
      # you can use the subcommand *write_appconfigs* to generate the config
      # files for Nginx(serve static assets) and Supervisord(manage application process).
      # The generated files will be in 'fuud/conf/_output'.
      # Copy the generated config files to the appropriate locations, and start the Nginx and
      # Supervisord processes. Check the 'setting/common.py' for help on setting the variables
      # used in rendering/parsing the config files.

# Application cache
     * The application cache MANIFEST is at 'static/offline.appcache'
     * Update the MANIFEST version whenever you update any of the cached items.
       This is how the client will detect changes and prompt the user to update
     * Chrome: chrome://appcache-internals/
     * Firefox: Clear Recent history and select offline data
     * Safari: In the menu bar, go to Safari > Empty Cache

# Queue
    The application uses [Django RQ](https://github.com/ui/django-rq) for processing large orders.

#Fabric Script
    To deploy on Heroku, set the environment variable HEROKU_APP, and
    the fabfile will pick it up.

##Environments
    fab development 'command'
    fab staging 'command'
    fab production 'command'

##Deployment
    fab production deploy
    fab production collectstatic
    fab brunchwatch
    fab brunchbuild

##DB
    fab staging resetdb
    fab schemamigration
    fab migrate
    fab updatedb

##Heroku
    fab production ps
    fab production restart
    fab production tail
    fab production shell
    fab production config


# TODO
see TODO file
